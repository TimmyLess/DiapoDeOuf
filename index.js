jQuery(document).ready(function ($) {


	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
    	$('#slider ul li:last-child').prependTo('#slider ul');
	
	$('.modal').modal();

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };
	
    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });
    $( "#edition" ).click(function() {
	  	var urlPicture = prompt("Setting the new picture url", "http://");
		if (urlPicture!=null)
		{
			console.log("change background-image of this li with :"+urlPicture);
			var textPicture = prompt("Setting the text", "exemple");
			if (textPicture!=null)
			{
				console.log("change data of this li with :"+textPicture);
			}
		}
	});
});



